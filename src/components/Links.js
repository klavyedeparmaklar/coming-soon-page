import React, { Component } from 'react'

import "../styles/Links.css";

class Links extends Component {
    render() {
        const { linkurl, linklogo } = this.props

        return (
            <div>
                <a target="_blank" href={linkurl}>
                    <img className="social-logo" src={linklogo} />
                </a>
            </div>
        )
    }
}

export default Links