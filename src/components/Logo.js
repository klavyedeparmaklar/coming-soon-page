import React, { Component } from 'react'

import '../styles/Logo.css'
import logo from '../images/logo.svg'

export default class Logo extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                </header>
            </div>
        )
    }
}
